import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  Switch,
  TextInput,
  TouchableHighlight,
  Button
} from "react-native";
import Constants from "expo-constants";

import Main from './app/components/Main';

export default class App extends React.Component {
  render() { 
    return (
      <Main />
    )
  }
}

// const DATA = [
//   {
//     id: '1',
//     title: 'First Item',
//     status: false
//   },
//   {
//     id: '2',
//     title: 'Second Item',
//     status: true
//   },
//   {
//     id: '3',
//     title: 'Third Item',
//     status: false
//   },
// ];

// export default function App() {
//   const renderItem = ({ item }) => (
//     <View style={{flex:1, flexDirection: 'row', justifyContent: 'space-between', margin:5}}>
//       <Text>{item.title}</Text>

//       <View style={{flexDirection: 'row', width: '30%', justifyContent: 'space-between'}}>
//         <Button
//           title="EDIT"
//           color="#4CAF50"
//         />

//         <Button
//           title="DONE"
//           color="#4CAF50"
//         />
//       </View>
//     </View>
//   );


//   const [isEnabled, setIsEnabled] = useState(false);
//   const [taskTxt, setText] = useState('');

//   const toggleSwitch = () => setIsEnabled(previousState => !previousState);

//   const addTask = () => {
//     DATA.push({
//       id: '4',
//       title: taskTxt,
//       status: false
//     });
//   }

//   return (
//     <SafeAreaView style={styles.container}>
//       <FlatList
//         style={{padding: 10}}
//         data={DATA}
//         renderItem={renderItem}
//         keyExtractor={item => item.id}
//       />

//       {/* <Switch
//         trackColor={{ false: "#767577", true: "#81b0ff" }}
//         thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
//         ios_backgroundColor="#3e3e3e"
//         onValueChange={toggleSwitch}
//         value={isEnabled}
//       /> */}

//       {/* FOOTER */}
//       {/* <View style={{flexDirection: 'row', alignItems: 'center'}}>
//         <TextInput
//           style={{ height: 50, width: '80%',  borderColor: 'gray', borderWidth: 1, borderRadius:15, padding:10, margin:10 }}
//           onChangeText={text => setText(text)}
//           placeholder={'Aa'}
//         />
//         <Button
//           onPress={addTask}
//           title="Add"
//           color="blue"
//         />
//       </View> */}
//     </SafeAreaView>

//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     marginTop: Constants.statusBarHeight,
//   }
// });
