import React from 'react';
import {
    Text,
    View,
    Button
} from "react-native";


export default class Task extends React.Component {
    render() {
        return (
            <View style={{flex:1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10, borderBottomWidth:1}}>
                <Text style={{fontSize:24, width:'65%'}}>{this.props.val.title}</Text>
                <View style={{flexDirection: 'row', width: '35%', justifyContent: 'space-between'}}>
                    <Button
                    title={this.props.val.status ? 'UNDONE' : 'DONE'}
                    onPress={this.props.doneTask}
                    color="blue"
                    />

                    <Button
                    title="EDIT"
                    onPress={this.props.editTask}
                    color="green"
                    />
                </View>
            </View>
        )
    }
}