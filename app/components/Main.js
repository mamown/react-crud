import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    TextInput,
    ScrollView,
    Switch,
    Button,
    Modal
} from "react-native";
import Constants from "expo-constants";

import Task from './Task.js';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            taskTxt: '',
            editTxt: '',
            activeIndex: '',
            done: false,
            unDone: false,
            modal: false,
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('tasks').then((data) => {
            if(data) this.setState({tasks: JSON.parse(data)});
        });
    }

    render() {
        //Loop all the tasks
        let taskComponent = this.state.tasks.map((val, key) => {
            if(this.state.done && this.state.unDone) {
                return <Task key={key} val={val} doneTask={() => this.doneTask(key)} editTask={() => this.editTask(key)}/>
            } else if(this.state.done) {
                if(!val.status) return <Task key={key} val={val} doneTask={() => this.doneTask(key)} editTask={() => this.editTask(key)}/>
            } else if(this.state.unDone) {
                if(val.status)  return <Task key={key} val={val} doneTask={() => this.doneTask(key)} editTask={() => this.editTask(key)}/>
            } else {
                return <Task key={key} val={val} doneTask={() => this.doneTask(key)} editTask={() => this.editTask(key)}/>
            }
        });

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerTxt}>TASKS</Text>
                </View>

                <ScrollView>
                    {/* Filter */}
                    <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                        <Text>DONE</Text>
                        <Switch
                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={this.state.done ? "#f5dd4b" : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(done) => this.setState({done})}
                            value={this.state.done}
                        />

                        <Text>UNDONE</Text>
                        <Switch
                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={this.state.unDone ? "#f5dd4b" : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={(unDone) => this.setState({unDone})}
                            value={this.state.unDone}
                        />
                    </View>
                    
                    {/* TASK */}
                    {taskComponent}
                </ScrollView>

                <View style={styles.footer}>
                    <TextInput
                    style={{ height: 50, width: '80%',  borderColor: 'gray', borderWidth: 1, borderRadius:15, padding:10, margin:10 }}
                    onChangeText={(taskTxt) => this.setState({taskTxt})}
                    value={this.state.taskTxt}
                    placeholder={'Aa'}
                    />
                    <Button
                    onPress={this.addTask.bind(this)}
                    title="Add"
                    color="black"
                    />
                </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modal}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <TextInput
                                style={{width:'40%', borderColor: 'gray', borderWidth: 1, padding:10, marginRight: 10}}
                                onChangeText={(editTxt) => this.setState({editTxt})}
                                value={this.state.editTxt}
                                placeholder={'Aa'}
                            />
                            <Button
                                onPress={(modal) => this.confirmEdit(modal)}
                                title="confirm"
                                color="blue"
                            />
                        </View>
                    </View>
                </Modal>

            </SafeAreaView>
        );
    }

    addTask() {
        if(this.state.taskTxt) {
            this.state.tasks.push({
                title: this.state.taskTxt,
                status: false,
            })

            this.setState({tasks: this.state.tasks})
            this.setState({taskTxt: ''});
            AsyncStorage.setItem('tasks', JSON.stringify(this.state.tasks));
        }
    }

    doneTask(index) {
        this.state.tasks[index].status = !this.state.tasks[index].status;
        this.setState({tasks: this.state.tasks});
        AsyncStorage.setItem('tasks', JSON.stringify(this.state.tasks));
    }

    editTask(index) {
        this.setState({modal: !this.state.modal});
        this.setState({editTxt: this.state.tasks[index].title});
        // storeIndex
        this.setState({activeIndex: index});
    }
    
    confirmEdit() {
        this.setState({modal: !this.state.modal});
        this.state.tasks[this.state.activeIndex].title = this.state.editTxt;
        this.setState({tasks: this.state.tasks});
        AsyncStorage.setItem('tasks', JSON.stringify(this.state.tasks));
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    height:70,
    backgroundColor: '#F31233'
  },   
  headerTxt: {
    color: 'white',
    fontSize:24
  },
  footer: {
    flexDirection: 'row', 
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  //Modal style
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    
  },
  modalView: {
    flexDirection: 'row', 
    alignItems: 'center',
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
